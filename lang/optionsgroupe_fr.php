<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_optionsgroupe' => 'Ajouter ce groupe d\'options',

	// C
	'champ_titre_label' => 'Libellé',
	'champ_obligatoire_label' => 'Rendre le choix d\'une option de ce groupe obligatoire',
	'confirmer_supprimer_optionsgroupe' => 'Confirmez-vous la suppression de cet groupe d\'options ?',

	// E
	'erreur_groupe_obligatoire' => 'Le choix d\'une option est nécessaire pour continuer'
	,
	// G
	'gerer_groupes' => 'Gérer les groupes d\'options',

	// I
	'icone_creer_optionsgroupe' => 'Créer un groupe d\'options',
	'icone_modifier_optionsgroupe' => 'Modifier ce groupe d\'options',
	'info_1_optionsgroupe' => 'Un groupe d\'options',
	'info_aucun_optionsgroupe' => 'Aucun groupe d\'options',
	'info_nb_optionsgroupes' => '@nb@ groupes d\'options',
	'info_optionsgroupes_auteur' => 'Les groupes d\'options de cet auteur',

	// R
	'retirer_lien_optionsgroupe' => 'Retirer ce groupe d\'options',
	'retirer_tous_liens_optionsgroupes' => 'Retirer tous les groupes d\'options',

	// S
	'supprimer_optionsgroupe' => 'Supprimer cet groupe d\'options',

	// T
	'texte_ajouter_optionsgroupe' => 'Ajouter un groupe d\'options',
	'texte_changer_statut_optionsgroupe' => 'Ce groupe d\'options est :',
	'texte_creer_associer_optionsgroupe' => 'Créer et associer un groupe d\'options',
	'texte_definir_comme_traduction_optionsgroupe' => 'Ce groupe d\'options est une traduction du groupe d\'options numéro :',
	'titre_langue_optionsgroupe' => 'Langue de ce groupe d\'options',
	'titre_logo_optionsgroupe' => 'Logo de ce groupe d\'options',
	'titre_objets_lies_optionsgroupe' => 'Liés à ce groupe d\'options',
	'titre_optionsgroupe' => 'Groupe d\'options',
	'titre_optionsgroupe_court' => 'Groupe',
	'titre_optionsgroupes' => 'Groupes d\'options',
	'titre_optionsgroupes_rubrique' => 'Groupes d\'options de la rubrique',
];
