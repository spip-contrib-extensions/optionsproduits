<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_au_panier' => 'Ajouter au panier',

	// C
	'cfg_objets' => 'Choisir les objets',
	'cfg_objets_explication' => 'Choisir les objets sur lesquels vous voulez activer des options.',
	'cfg_titre_parametrages' => 'Paramétrages',

	// G
	'gerer_options' => 'Gérer les options',

	// O
	'options_titre' => 'Options',

	// P
	'panier_prix_total' => 'Dans le formulaire d\'ajout au panier, afficher le prix total au lieu du prix de l\'option',
	'prix_a_partir_de' => 'A partir de',

	// Q
	'quantite' => 'Quantité',

	// T
	'titre_page_configurer_options' => 'Configuration des options',

	// U
	'gerer_poids' => 'Gérer le poids des options',
];
