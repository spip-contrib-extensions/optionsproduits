<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_option' => 'Ajouter cette option',
	'aucune' => 'Aucune',

	// C
	'champ_description_label' => 'Description',
	'champ_id_optionsgroupe_label' => 'Groupe',
	'champ_prix_defaut_explication' => '(peut être modifié pour chaque association avec un objet)',
	'champ_poids_defaut_explication' => '(en grammes , peut être modifié pour chaque association avec un objet)',
	'champ_poids_defaut_label' => 'Poids par défaut',
	'champ_prix_defaut_label' => 'Prix HT par défaut',
	'champ_prix_defaut_ttc_label' => 'Prix TTC par défaut',
	'champ_titre_label' => 'Libellé',
	'confirmer_supprimer_option' => 'Confirmez-vous la suppression de cette option ?',

	// E
	'explication_laisser_prix_vide' => 'Laisser vide pour utiliser le prix par défaut',
	'explication_laisser_poids_vide' => 'Laisser vide pour utiliser le poids par défaut',
	'editer_ttc' => 'Prix des options sur les objets saisis en TTC',

	// I
	'icone_creer_option' => 'Créer une option',
	'icone_modifier_option' => 'Modifier cette option',
	'info_1_option' => 'Une option',
	'info_1_objet' => 'Un objet lié',
	'info_aucun_option' => 'Aucune option',
	'info_options_auteur' => 'Les options de cet auteur',
	'info_nb_options' => '@nb@ options',
	'info_nb_objets' => '@nb@ objets liés',

	// M
	'modifier' => 'Modifier',

	// O
	'option_ajoutee' => 'Cette option a été ajoutée.',

	// P
	'prix_option_ht' => 'Prix de l\'option HT',
	'prix_option_ttc' => 'Prix de l\'option TTC',
	'prix_objet_ttc' => 'Prix de l\' objet TTC',
	'prix_final_ht' => 'Prix final HT',
	'prix_final_ttc' => 'Prix final TTC',
	'poids' => 'Poids',
	'poids_option' => 'Poids de l\'option',
	'poids_final' => 'Poids final',

	// R
	'retirer' => 'Retirer',
	'retirer_lien_option' => 'Retirer cette option',
	'retirer_tous_liens_options' => 'Retirer toutes les options',

	// S
	'supprimer_option' => 'Supprimer cette option',

	// T
	'texte_ajouter_option' => 'Ajouter une option',
	'texte_changer_statut_option' => 'Cette option est :',
	'texte_creer_associer_option' => 'Créer et associer une option',
	'texte_definir_comme_traduction_option' => 'Cette option est une traduction de la option numéro :',
	'titre' => 'Option',
	'titres' => 'Options de produits',
	'titres_court' => 'Options',
	'titre_options' => 'Options',
	'titres_rubrique' => 'Options de la rubrique',
	'titre_langue_option' => 'Langue de cette option',
	'titre_logo_option' => 'Logo de cette option',
	'titre_objets_lies_option' => 'Liés à cette option',
];
