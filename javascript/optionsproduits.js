jQuery(function(){

	function updatePrixOptionObjet(form) {
		const labelPrix = form.find('.js-prix_objet_valeur');
		// calculer le prix du objet + des options
		let prixObjet = labelPrix.data('prix')||0;
		let prixChanged = false;
		form.find('.editer_options_objet input[type=radio]:checked').each(function(){
			prixChanged = true;
			prixObjet += parseFloat($(this).data('prixoption'))||0;
		});
		if(prixChanged) {
			// TODO : pouvoir utiliser une autre monnaie que l'euro
			prixObjet = prixObjet.toFixed(2).replace('\.', ',') + ' €';
			labelPrix.html('<span class="montant">' + prixObjet + '</span>');
		}
	}

	$('.formulaire_panier_options').each(function(){
		const form = $(this);
		// mise à jour du prix affiché en fonction du choix des options
		updatePrixOptionObjet(form);
		form.find('.editer_options_objet input[type=radio]').on('click', function() {
			updatePrixOptionObjet(form);
		});
	});

});
