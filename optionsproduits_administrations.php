<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Optionsproduits
 *
 * @plugin     Optionsproduits
 * @copyright  2017
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Optionsproduits\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation et de mise à jour du plugin Optionsproduits.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
 **/
function optionsproduits_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [

		// créer les tables des options
		[
			'maj_tables',
			[
				'spip_options',
				'spip_options_liens',
				'spip_optionsgroupes',
			],
		],

		// ajouter le champ options dans les tables des plugins Panier et Commande
		['optionsproduits_alter_paniers_commandes'],

		// ajouter les options et groupes à la config des objets géré par Rang
		['optionsproduits_configure_rang'],
	];

	$maj['1.0.1'] = [
		['optionsproduits_configure_rang'],
	];

	$maj['1.0.2'] = [
		["sql_alter", "table spip_paniers_liens DROP PRIMARY KEY"],
		["sql_alter", "table spip_paniers_liens ADD options varchar(255) NOT NULL DEFAULT ''"],
		["sql_alter", "table spip_paniers_liens ADD PRIMARY KEY (id_panier, id_objet, objet, options)"],
		["sql_alter", "table spip_commandes_details ADD options varchar(255) NOT NULL DEFAULT ''"],
	];

	$maj['1.0.3'] = [
		['optionsproduits_defaut_produits'],
	];

	$maj['1.1.0'] = [['maj_tables', ['spip_options', 'spip_options_liens']]];
	$maj['1.1.1'] = [['maj_tables', ['spip_optionsgroupes']]];

	$maj['1.1.2'] = [
		['optionsproduits_update_options_panier'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin Optionsproduits.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 *
 * @return void
 **/
function optionsproduits_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_options');
	sql_drop_table('spip_options_liens');
	sql_drop_table('spip_optionsgroupes');

	// Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', ['option']));
	sql_delete('spip_versions_fragments', sql_in('objet', ['option']));

	effacer_meta($nom_meta_base_version);

	// Retirer les options et groupes d'options dans la liste des objets géré par le plugin Rang
	$tables = lire_config('rang/rang_objets');
	$tables = explode(',', $tables);
	unset($tables['spip_options']);
	unset($tables['spip_optionsgroupes']);
	ecrire_config('rang/rang_objets', implode(',', $tables));
}

/**
 * Ajouter les options et groupes d'options dans la liste des objets géré par le plugin Rang
 *
 * @return void
 **/
function optionsproduits_configure_rang() {
	$tables = lire_config('rang/rang_objets');
	$tables = explode(',', $tables);
	$tables_options = [
		'spip_options',
		'spip_optionsgroupes',
	];
	$tables = array_unique(array_merge($tables, $tables_options));
	ecrire_config('rang/rang_objets', implode(',', $tables));
	// créer les champs 'rang' dans les tables
	rang_creer_champs($tables_options);
}

/**
 * Ajouter le champ options dans les tables des plugins Panier et Commande
 *
 * @return void
 **/
function optionsproduits_alter_paniers_commandes() {
	if (test_plugin_actif('paniers')) {
		// ajouter un champ pour les options dans les lignes du panier
		sql_alter('TABLE spip_paniers_liens ADD options VARCHAR(100) NOT NULL DEFAULT ""');
		// recréer la clé du panier avec les options
		sql_alter('TABLE spip_paniers_liens DROP PRIMARY KEY');
		sql_alter('TABLE spip_paniers_liens ADD PRIMARY KEY (id_panier, id_objet, objet, options)');
	}
	if (test_plugin_actif('commandes')) {
		// ajouter un champ pour les options dans les lignes des commandes
		sql_alter('TABLE spip_commandes_details ADD options VARCHAR(100) NOT NULL DEFAULT ""');
	}
}

/**
 * Déclare les options sur les produits à l'installation
 *
 * @return void
 */
function optionsproduits_defaut_produits() {
	$objets = explode(',', lire_config('optionsproduits/objets'));
	$objets[] = 'spip_produits';
	$objets = array_filter(array_unique($objets));
	ecrire_config('optionsproduits/objets', implode(',', $objets));
}

/**
 * Mise à jour du caractère séparateur des options dans les paniers et les commandes
 *
 * @return void
 */
function optionsproduits_update_options_panier() {
	sql_update('spip_paniers_liens', ['options' => 'REPLACE(options, "|", "_")']);
	sql_update('spip_commandes_details', ['options' => 'REPLACE(options, "|", "_")']);
}
