<?php
/**
 * Utilisation de l'action supprimer pour l'objet option
 *
 * @plugin     Optionsproduits
 * @copyright  2017
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Optionsproduits\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour supprimer un groupe d'options
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $id_optionsgroupe
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
 **/
function action_supprimer_optionsgroupe_dist($id_optionsgroupe = null) {
	if (is_null($id_optionsgroupe)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_optionsgroupe = $securiser_action();
	}
	$id_optionsgroupe = intval($id_optionsgroupe);

	// cas suppression
	if ($id_optionsgroupe && autoriser('supprimer', 'optionsgroupe', $id_optionsgroupe)) {
		sql_delete('spip_optionsgroupes', 'id_optionsgroupe=' . sql_quote($id_optionsgroupe));
	} else {
		spip_log("action_supprimer_optionsgroupe_dist $id_optionsgroupe pas compris");
	}
}
