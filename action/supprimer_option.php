<?php
/**
 * Utilisation de l'action supprimer pour l'objet option
 *
 * @plugin     Optionsproduits
 * @copyright  2017
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Optionsproduits\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour supprimer une option
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $id_option
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
 **/
function action_supprimer_option_dist($id_option = null) {
	if (is_null($id_option)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_option = $securiser_action();
	}
	$id_option = intval($id_option);

	// cas suppression
	if ($id_option && autoriser('supprimer', 'option', $id_option)) {
		sql_delete('spip_options', 'id_option=' . sql_quote($id_option));
	} else {
		spip_log("action_supprimer_option_dist $id_option pas compris");
	}
}
