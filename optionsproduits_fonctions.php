<?php
/**
 * Fonctions utiles au plugin Optionsproduits
 *
 * @plugin     Optionsproduits
 * @copyright  2017
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Optionsproduits\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Convertit une valeur postée en chaine
 *
 * @param array|string $options_produit
 * @return string
 */
function options_produit_request_to_string($options_produit): string {
	return is_string($options_produit) ?
		$options_produit
		: join('_', array_filter($options_produit));
}

/**
 * Convertit une chaine d'options en tableau
 *
 * @param string $options_produit
 * @return array
 */
function options_produit_to_array(string $options_produit): array {
	return explode('_', trim($options_produit, '_'));
}

// Un filtre pour obtenir le poids final d'un objet
function poids_final_objet($id_objet, $objet, $options_produit = null) {
	static $cache;
	$hash = md5(json_encode([$id_objet, $objet, $options_produit]));
	if (isset($cache[$hash])) {
		return $cache[$hash];
	}

	$poids_objet = (float)generer_info_entite($id_objet, $objet, 'poids');
	if (!$poids_objet) {
		$poids_objet = (float)generer_info_entite($id_objet, $objet, 'weight');
	}
	include_spip('optionsproduits_fonctions');
	$options_produit = options_produit_to_array($options_produit, '_');
	foreach ($options_produit as $option) {
		$poids_objet += sql_getfetsel(
			'poids_option_objet',
			'spip_options_liens',
			[
				'id_option = ' . intval($option),
				'objet = ' . sql_quote($objet),
				'id_objet = ' . intval($id_objet),
			]
		);
	}
	$cache[$hash] = $poids_objet;

	return $poids_objet;
}

// Calcul du poids d'un objet avec ses options
function balise_POIDS_FINAL_OBJET_dist($p) {
	$b = $p->nom_boucle ? $p->nom_boucle : $p->descr['id_mere'];
	if (!$objet = interprete_argument_balise(1, $p)) {
		$objet = sql_quote($p->boucles[$b]->type_requete);
		$id_objet = champ_sql($p->boucles[$b]->primary, $p);
	} else {
		$id_objet = interprete_argument_balise(2, $p);
		$_options = interprete_argument_balise(3, $p);
	}
	$p->code = "poids_final_objet(intval(" . $id_objet . ")," . $objet . "," . $_options . ")";
	$p->interdire_scripts = false;

	return $p;
}
