<?php
/**
 * & * Surcharge de SPIP\Panier\Prix\Panier pour prendre en compte les options des produits
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Le prix HT d'un panier : addition des prix HT des objets liés avec leurs options
function prix_panier_ht_dist($id_panier, $ligne, $options = []) {
	$fonction_prix = charger_fonction('ht', 'inc/prix');
	return _prix_panier_iterer_options($id_panier, $fonction_prix, $options);
}

// Le prix TTC d'un panier : addition des prix TTC des objets liés avec leurs options
function prix_panier_dist($id_panier, $prix_ht, $options = []) {
	$fonction_prix = charger_fonction('prix', 'inc/');
	return _prix_panier_iterer_options($id_panier, $fonction_prix, $options);
}

// Fonction générique "privée" qui itère sur des options pour les ajouter à un prix
function _prix_panier_iterer_options($id_panier, $fonction_prix, $options) {
	$prix = 0;
	// On va chercher tous les objets liés
	$objets = sql_allfetsel('objet, id_objet, quantite, reduction, options', 'spip_paniers_liens', 'id_panier = ' . intval($id_panier));
	// Pour chaque objet on va chercher son prix HT x sa quantité
	if (is_array($objets)) {
		foreach ($objets as $objet) {
			$p = $fonction_prix(
					$objet['objet'],
					$objet['id_objet'],
					array_merge($options, ['options_produit' => $objet['options']])
				) * $objet['quantite'];
			if (
				isset($objet['reduction'])
				and ($reduction = floatval($objet['reduction'])) > 0
			) {
				$reduction = min($reduction, 1.0); // on peut pas faire une reduction de plus de 100%;
				$p = $p * (1.0 - $reduction);
			}

			$prix += $p;
		}
	}
	return $prix;
}
