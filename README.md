# Gérer des options sur des produits

## v3

Refactoring de l'api pour se recaler sur le plugin prix
Attention : rupture de compatibilité

Les balises `#PRIX_OPTION` et `#PRIX_OPTION_HT` sont supprimées, on utilise maintenant les balises du plugin Prix (`#PRIX` et `#PRIX_HT`) en leur passant dans le troisième argument les options des produits.

Exemple : à la place de `#PRIX_OPTION{produit,#ID_PRODUIT,#ID_OPTION}`, on utilise maintenant `#PRIX{produit,#ID_PRODUIT,#ARRAY{options_produit,#ID_OPTION}}`

Idem en PHP :

```php
$fonction_prix = charger_fonction('prix', 'inc/');
$fonction_prix(
  $objet['objet'],
  $objet['id_objet'],
  array_merge($options, ['options_produit' => $objet['options']])
)
```

## Notes techniques

A l'installation, ajout d'un champ 'options varchar(100)' dans les tables spip_paniers_liens et spip_commandes_details

La clé primaire composée de spip_paniers_liens est supprimée et recréée avec ces champs : (id_panier, id_objet, objet, options)

## Surcharges du plugin panier

Pour tenir compte des options de produits

- action/commandes_paniers.php
- action/remplir_panier.php
- formulaires/panier.html
- formulaires/panier.php
- prix/panier.php

## TODO

### Prendre en compte `#FORMULAIRE_REMPLIR_PANIER`

Pour l'instant, les options n'y sont pas prises en compte.
Faire une surcharge, comme pour `#FORMULAIRE_PANIER`?

### Choix des options

Actuellement, côté public, on ne peut choisir qu'une option par groupe.

Proposer une configuration pour chaque groupe, qui permettrait d'en choisir soit une seule soit plusieurs (radio/checkbox).

### Problème à l'installation

Si la table spip_paniers_liens contient déjà des données, la création de la nouvelle primary key composée génère une erreur sur un serveur Mysql (Duplicate entry ...).

Deux solutions :

1/ vider la table spip_paniers_liens

2/ passer par une clé autoincrement temporaire :

ALTER TABLE `spip_paniers_liens` ADD `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE `spip_paniers_liens` ADD UNIQUE KEY  (`id_panier`, `id_objet`, `objet`, `options`);
ALTER TABLE `spip_paniers_liens` DROP `id`;

mais ça ne marche pas sur tous les serveurs Mysql

## Versions précédentes


### v2.0.0

Compatible SPIP 4.0 et 4.1
La branche v1 reste pour compatibilité SPIP 3.2

### v1.4.0

On peut activer dans la config la gestion du poids des options, qui s'ajoute alors aux produits (comme les prix).
Un champ 'obligatoire' sur les groupes d'options, qui oblige côté front à choisir au moins une de ses options pour l'ajout au panier.
Version stable, utilisée en prod sur plusieurs sites.

### v1.3.2

Les options peuvent être associées à tous les objets éditoriaux, pas uniquement aux produits.
L'association se fait dans la configuration du plugin (liste des objets à cocher).
Les plugins produits et prix deviennent donc optionnels.
Pour gérer des options sur un objet patate, on pourra par exemple ajouter un champ prix (et taxe) dans la définition de l'objet patate.

Un formulaire générique permet d'ajouter un objet et ses options au panier.
Exemple : #FORMULAIRE_PANIER_OPTIONS{patate, #ID_PATATE} à utiliser dans une <boucle_(PATATES)>

### v1.0.0

Le plugin gère des groupes d'options, dans lesquels on crée des options.

Chaque option a un prix HT par défaut (positif ou négatif), qui s'ajoute au prix de base du produit.

Sur chaque produit, on choisit quelles options on lui affecte.
On peut aussi modifier le prix de l'option sur le produit, pour avoir un prix différent du prix par défaut de l'option.

Côté public, les options sont proposées sous forme de boutons radio, classées par groupes.
Le prix HT des options est ajouté au prix HT du produit, et la TVA s'applique donc sur le total.
Un script JS mets à jour visuellement le prix TTC du produit en fonction des options choisies.

Les options sont transmises aux paniers puis aux commandes.
La surcharge de formulaires/panier permet d'afficher le nom du produit avec toutes les options choisies.
