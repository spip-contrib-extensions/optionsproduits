<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_liens');

function formulaires_options_liees_objet_charger_dist($objet, $id_objet) {
	$valeurs = [
		'objet' => $objet,
		'id_objet' => $id_objet,
		'id_option' => _request('id_option'),
		'modifier_option' => _request('modifier_option'),
		'poids' => _request('poids'),
	];

	if (_request('modifier_option') && _request('id_option') && lire_config('optionsproduits/editer_ttc')) {
		$infos_option = sql_fetsel(
			'prix_option_objet, poids_option_objet',
			'spip_options_liens',
			'id_option = ' . _request('id_option') . ' and objet=' . sql_quote($objet) . ' and id_objet=' . $id_objet
		);
		$table = table_objet_sql($objet);
		$desc = description_table($table);
		if (isset($desc['field']['taxe'])) {
			$taxe = sql_getfetsel('taxe', $table, id_table_objet($table) . '=' . $id_objet);
			$valeurs['prix'] = $infos_option['prix_option_objet'] * (1 + $taxe);
			if (lire_config('optionsproduits/editer_ttc')) {
				$valeurs['prix'] = round($valeurs['prix'], 2);
			}
			$valeurs['ttc'] = true;
		} else {
			$valeurs['prix'] = $infos_option['prix_option_objet'];
			$valeurs['ttc'] = false;
		}
		$valeurs['poids'] = $infos_option['poids_option_objet'];
	}

	return $valeurs;
}

function formulaires_options_liees_objet_verifier_dist($objet, $id_objet) {
	$erreurs = [];

	if (_request('prix') && !is_numeric(_request('prix'))) {
		$erreurs['prix'] = 'Saisissez un nombre décimal avec un point';
	}

	return $erreurs;
}

function formulaires_options_liees_objet_traiter_dist($objet, $id_objet) {
	$retours = [];

	$taxe = 0;
	if (_request('ttc') || lire_config('optionsproduits/editer_ttc')) {
		$table = table_objet_sql($objet);
		$desc = description_table($table);
		if (isset($desc['field']['taxe'])) {
			$taxe = sql_getfetsel('taxe', $table, id_table_objet($table) . '=' . $id_objet);
		}
	}

	// modification
	if (_request('modifier_option')) {
		if ($id_option = _request('id_option')) {
			$prix = _request('prix');
			if ($prix === '') {
				$prix = sql_getfetsel('prix_defaut', 'spip_options', 'id_option = ' . $id_option);
			}
			$poids = _request('poids');
			if ($poids === '') {
				$poids = sql_getfetsel('poids_defaut', 'spip_options', 'id_option = ' . $id_option);
			}
			if (objet_associer(
				['option' => $id_option],
				[$objet => $id_objet],
				[
					'prix_option_objet' => $prix / (1 + $taxe),
					'poids_option_objet' => $poids,
				]
			)) {
				$retours['message_ok'] = _T('option:option_ajoutee');
			}
		}
		set_request('modifier_option', '');

	} else {

		// association d'options
		$groupes = sql_allfetsel('id_optionsgroupe', 'spip_optionsgroupes');
		foreach ($groupes as $groupe) {
			if ($id_option = _request('id_option_groupe_' . $groupe['id_optionsgroupe'])) {
				$infos_options = sql_fetsel('prix_defaut, poids_defaut', 'spip_options', 'id_option = ' . $id_option);
				if (objet_associer(
					['option' => $id_option],
					[$objet => $id_objet],
					[
						'prix_option_objet' => $infos_options['prix_defaut'] / (1 + $taxe),
						'poids_option_objet' => $infos_options['poids_defaut'],
					]
				)) {
					$retours['message_ok'] = _T('option:option_ajoutee');
				}
			}
			set_request('id_option_groupe_' . $groupe['id_optionsgroupe'], '');
		}

	}

	return $retours;
}
