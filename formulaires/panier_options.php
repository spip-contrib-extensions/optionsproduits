<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_panier_options_charger_dist($objet, $id_objet) {
	$valeurs = [
		'objet' => $objet,
		'id_objet' => $id_objet,
	];

	// Calculer le prix de l'objet pour le passer au formulaire
	$fonction_prix = charger_fonction('prix', 'inc/');
	$fonction_prix_ht = charger_fonction('ht', 'inc/prix');
	$valeurs['prix_ht'] = $fonction_prix_ht($objet, $id_objet);
	$valeurs['prix'] = $fonction_prix($objet, $id_objet);

	// On transmet les options de produits reçues au formulaire sous forme concaténee
	// pour qu'il les réaffiche comme cochées, mais on ne lui transmet pas les
	// id_options reçues pour qu'il ne les remette pas dans #ACTION_FORMULAIRE
	$options_produit = [];
	foreach ($_REQUEST as $key => $value) {
		if ($value && strpos($key, 'id_option') !== false) {
			set_request($key, '');
			$options_produit[] = intval($value);
		}
	}
	include_spip('optionsproduits_fonctions');
	$valeurs['options'] = options_produit_request_to_string($options_produit);

	return $valeurs;
}

function formulaires_panier_options_verifier_dist($objet, $id_objet) {
	$erreurs = [];
	$groupes = sql_allfetsel('id_optionsgroupe, obligatoire', 'spip_optionsgroupes', 'obligatoire = 1');
	foreach ($groupes as $groupe) {
		$options_groupes = sql_allfetsel(
			'o.id_option',
			'spip_options o join spip_options_liens ol using(id_option)',
			'o.id_optionsgroupe = ' . $groupe['id_optionsgroupe'] . ' and ol.objet = ' . sql_quote($objet) . ' and ol.id_objet = ' . $id_objet
		);
		if ($options_groupes && !_request("id_option_{$groupe['id_optionsgroupe']}_{$objet}_{$id_objet}")) {
			$erreurs['id_groupe' . $groupe['id_optionsgroupe']] = _T('optionsgroupe:erreur_groupe_obligatoire');
		}
	}

	return $erreurs;
}

function formulaires_panier_options_traiter_dist($objet, $id_objet) {
	// On récupère les infos
	$quantite = intval(_request("quantite_{$objet}_{$id_objet}"));
	if(!$quantite) {
		$quantite = 1;
	}
	$negatif = '';
	$options_produit = [];

	// On reçoit des options de produits en POST sous la forme id_option
	// ou id_optionX où X est l'id du groupe d'options.
	$groupes = sql_allfetsel('id_optionsgroupe', 'spip_optionsgroupes');
	foreach ($groupes as $groupe) {
		if ($id_option = _request("id_option_{$groupe['id_optionsgroupe']}_{$objet}_{$id_objet}")) {
			$options_produit[] = $id_option;
		}
	}
	// On concatène pour passer les options de produits à l'action remplir_panier.
	include_spip('optionsproduits_fonctions');
	$options_produit = options_produit_request_to_string($options_produit);

	// On appelle l'action remplir_panier
	$remplir_panier = charger_fonction('remplir_panier', 'action');
	$data = $remplir_panier($objet . '-' . $id_objet . '-' . $quantite . '-' . $negatif . '-' . $options_produit);

	// JS dans le message de retour
	$data = json_encode($data); // objet, id_objet, id_panier, quantite, negatif, options
	// Déclencher un évènement
	$js_event = "
		var data = $data;
		$( \"body\" ).trigger( \"remplir_panier\", data );
	";
	$js_ajaxreload = "
		ajaxReload('minipanier');
	";
	// Emballons tout ça
	return ['message_ok' => "
		<script>
			$(function($) {
				$(function() {
					$js_event
					$js_ajaxreload
				});
			});
		</script>
	"];
}
